#!/usr/bin/ruby -w

#ARGS in order:
##0. businessName 1. businessSite 2. scoreGrade 3. alexaRanking 4. alexaPageViewsPerMil
##5. seoBackLinks 6. seoMozScore 7. seoPageAuthority 8. seoDomainAuthority 9. socialTwitterHandle
##10. socialTweet 11. socialFollowers
businessName = ARGV[0]
businessSite = ARGV[1]
scoreGrade = ARGV[2]
socialTwitterHandle = ARGV[3]
socialRecentTweet = ARGV[4]
##will switch to calculated row when data is available in a month and switch to new followers vs overall followers
socialFollowers = ARGV[5]
alexaRanking = ARGV[6]
alexaPageViewsPerMil = ARGV[7]
seoBacklinks = ARGV[8]
seoMozScore = ARGV[9]
seoPageAuthority = ARGV[10]
seoDomainAuthority = ARGV[11]

name = ARGV[12]
fname = name + ".html"
##Use the same method as two lines below and edits the css file depending on what seasonal is
seasonal = nil
outputfile = File.open("html_docs/" + fname, "w+")
outputfile.puts %Q(<!DOCTYPE html>
<html>
    <head>
        <title>IN PROGRESS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="marketing.css">
    </head>
    <body>
    	<div id="header_container">
        	<div id="header_with_logo">
        	<div id="business_intro">#{businessName}<br>HERE'S WHERE<br>
        	</div>
        	YOU RANK
        	</div>
        </div>
        <div id="block_header">YOUR INTERNET SCORECARD</div>
        <div id="block_container">
            <div id="score_block">
            	<div class="blockIcons" id="scoreIcon"></div>
                <div class="blockTitle" id="score_block_title">ONLINE MARKETING SCORE</div>
                <div id="score_block_grade">#{scoreGrade}</div>
                <div id="score_block_company">#{businessSite}</div>
            </div>
            <div id="security_block">
            	<div class="blockIcons" id="securityIcon"></div>
                <div class="blockTitle" id="security_block_title">YOUR WEBSITE ALEXA RANKING</div>
                <div class="formattedList" id="security_block_threat_level"><a style="color:black;font-weight:bold;">Ranking..................................................</a>#{alexaRanking}</div>
                <div class="formattedList" id="security_block_security_issues"><a style="color:black;font-weight:bold;">Page Views per Million.........................</a>#{alexaPageViewsPerMil}</div>
            </div>
            <div id="seo_block">
            	<div class="blockIcons" id="seoIcon"></div>
                <div class="blockTitle" id="seo_block_title">YOUR SEO REPORT</div>
                <div class="formattedList" id="seo_block_backlinks"><a style="color:black;font-weight:bold;">Backlinks...........................</a>#{seoBacklinks}</div>
                <div class="formattedList" class="formattedList" id="seo_block_backlinks_past_60"><a style="color:black;font-weight:bold;">Moz Score..........................</a>#{seoMozScore}</div>
                <div class="formattedList" id="seo_block_top_domains"><a style="color:black;font-weight:bold;">Page Authority...................</a>#{seoPageAuthority}</div>
                <div class="formattedList" id="seo_block_top_pages"><a style="color:black;font-weight:bold;">Domain Authority..............</a>#{seoDomainAuthority}</div>
            </div>
            <div id="social_block">
            	<div class="blockIcons" id="socialIcon"></div>
                <div class="blockTitle" id="social_block_title">YOUR SOCIAL MEDIA</div>
                <div id="social_block_twitter"><div id="twtrlogo"></div>@#{socialTwitterHandle} <a style="color:black">Your most recent tweet had #{socialRecentTweet} favorites.</a></div>
                <div id="social_block_new_followers">You Have <a style="color:red;font-style:italic;font-weight:bold">#{socialFollowers}</a> followers.</div>
            </div>
        </div>

        <div id="outro"></div>
        <div id="footer_with_logo"></div>
        <div></div>

    </body>
</html>)

outputfile.close

puts "Hello Ruby"
